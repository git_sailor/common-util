package com.itasura.guava.Concurrency;

import com.google.common.util.concurrent.FutureFallback;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

/**
 * @author sailor wang
 * @date 2018/10/30 5:55 PM
 * @description
 */
public class FutureFallbackImpl implements FutureFallback<String> {
    @Override
    public ListenableFuture<String> create(Throwable throwable) throws Exception {
        if (throwable instanceof RuntimeException) {
            SettableFuture<String> settableFuture = SettableFuture.create();
            settableFuture.set("404, File Not Found");
            settableFuture.setException(throwable);
            return settableFuture;
        } else {
            throw new Exception(throwable);
        }
    }
}