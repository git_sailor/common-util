package com.itasura.guava.Concurrency;

import com.google.common.util.concurrent.FutureCallback;

/**
 * @author sailor wang
 * @date 2018/10/30 2:57 PM
 * @description
 */
public class FutureCallbackImpl implements FutureCallback<String> {

    private StringBuilder sb = new StringBuilder();

    @Override
    public void onSuccess(String s) {
        sb.append("success -> ").append(s);
    }

    @Override
    public void onFailure(Throwable throwable) {
        sb.append("failure -> ").append(throwable.toString());
    }

    public String getCallbackResult() {
        return sb.toString();
    }
}