package com.itasura.guava.Concurrency;

import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/30 5:00 PM
 * @description
 */
@Slf4j
public class AsyncFunctionTest {

    @Test
    public void asyncFunctionTest() throws Exception {
        AsyncFuntionSample asyncFunction = new AsyncFuntionSample();
        ListenableFuture<String> listenableFuture1 = asyncFunction.apply(1);
        log.info("listenableFuture -> {}",listenableFuture1.get());
        ListenableFuture<String> listenableFuture2 = asyncFunction.apply(1);
        log.info("listenableFuture -> {}",listenableFuture2.get());
    }
}