package com.itasura.guava.Concurrency;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

/**
 * @author sailor wang
 * @date 2018/10/30 3:17 PM
 * @description
 */
@Slf4j
public class FutureCallbackTest {

    @Test
    public void futureCallbackTest() {
        ListeningExecutorService es = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(3));
        ListenableFuture<String> listenableFuture = es.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return SimpleService.visitDb();
            }
        });

        FutureCallbackImpl callback = new FutureCallbackImpl();
        Futures.addCallback(listenableFuture,callback);
        String result = callback.getCallbackResult();
        log.info("result -> {}",result);

    }

}