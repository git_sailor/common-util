    1. ListenableFuture：该接口扩展了Future接口，增加了addListener方法，该方法在给定的excutor上注册一个监听器，当计算完成时会马上调用该监听器。不能够确保监听器执行的顺序，但可以在计算完成时确保马上被调用。 
    2. FutureCallback：该接口提供了OnSuccess和onFailure方法。获取异步计算的结果并回调。 
    3. MoreExecutors：该类是final类型的工具类，提供了很多静态方法。例如listeningDecorator方法初始化ListeningExecutorService方法，使用此实例submit方法即可初始化ListenableFuture对象。 
    4. ListenableFutureTask：该类是一个适配器，可以将其它Future适配成ListenableFuture。 
    5. ListeningExecutorService：该类是对ExecutorService的扩展，重写了ExecutorService类中的submit方法，并返回ListenableFuture对象。 
    6. JdkFutureAdapters：该类扩展了FutureTask类并实现ListenableFuture接口，增加了addListener方法。 
    7. Futures：该类提供和很多实用的静态方法以供使用。