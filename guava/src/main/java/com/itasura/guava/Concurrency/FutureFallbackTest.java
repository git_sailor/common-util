package com.itasura.guava.Concurrency;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

/**
 * @author sailor wang
 * @date 2018/10/30 5:51 PM
 * @description
 */
@Slf4j
public class FutureFallbackTest {

    @Test
    public void futureFallbackTest() throws ExecutionException, InterruptedException {
        ListeningExecutorService es = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        ListenableFuture<String> listenableFuture = es.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                throw new RuntimeException();
            }
        });

        FutureFallbackImpl fallback = new FutureFallbackImpl();
        Futures.withFallback(listenableFuture, fallback);
        log.info("result -> {}", listenableFuture.get());

    }
}