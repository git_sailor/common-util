package com.itasura.guava.Concurrency;

import lombok.extern.slf4j.Slf4j;

/**
 * @author sailor wang
 * @date 2018/10/30 12:07 PM
 * @description
 */
@Slf4j
public class SimpleService {

    public static String visitDb() {
        return "模拟dao请求";
    }

    public static String visitBlockDb() throws InterruptedException {
        Thread.sleep(2000);
        return "模拟dao阻塞请求";
    }

    public static String visitDbFail() throws Exception {
        throw new Exception("模拟接口异常");
    }
}