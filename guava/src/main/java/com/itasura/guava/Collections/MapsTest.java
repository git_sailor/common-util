package com.itasura.guava.Collections;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.itasura.guava.BasicGuavaUtilities.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sailor wang
 * @date 2018/10/19 下午5:23
 * @description
 */
@Slf4j
public class MapsTest {
    static Map<Long, User> userMap = Maps.newHashMap();
    static List<User> userList;

    static {
        User u1 = new User();
        u1.setId(1L);
        u1.setName("A");
        u1.setAge(1);
        userMap.put(u1.getId(), u1);
        User u2 = new User();
        u2.setId(1L);
        u2.setName("B");
        u2.setAge(13);
        userMap.put(u2.getId(), u2);
        User u3 = new User();
        u3.setId(3L);
        u3.setName("C");
        u3.setAge(15);
        userMap.put(u3.getId(), u3);
        userList = Lists.newArrayList(u1, u2, u3);
    }

    // 唯一index
    @Test
    public void uniqueIndexTest() {
        Map<Long, User> uMap = Maps.uniqueIndex(userList.iterator(), new Function<User, Long>() {
            @Override
            public Long apply(User user) {
                return user.getId();
            }
        });
        log.info("uMap -> {}", uMap);
    }

    // 作用和uniqueIndex相反
    @Test
    public void asmapTest() {
        User u1 = new User();
        u1.setId(1L);
        u1.setName("A");
        u1.setAge(1);
        User u2 = new User();
        u2.setId(2L);
        u2.setName("B");
        u2.setAge(13);
        User u3 = new User();
        u3.setId(3L);
        u3.setName("C");
        u3.setAge(15);
        Set<User> userSet = Sets.newHashSet(u1, u2, u3);
        Map<User, Long> userMap = Maps.asMap(userSet, new Function<User, Long>() {
            @Override
            public Long apply(User user) {
                return user.getId();
            }
        });
        log.info("userMap -> {}", userMap);
    }

    @Test
    public void entryTransformerTest() {
        Map<String, Boolean> fromMap = Maps.newHashMap();
        fromMap.put("key", true);
        fromMap.put("value", false);

        Map<String, Object> reverseMap = Maps.transformValues(fromMap, new Function<Boolean, Object>() {
            @Override
            public Object apply(Boolean input) {
                //对传入的元素取反
                return !input;
            }
        });
        log.info("entryTransformer -> {}", reverseMap);

        Maps.EntryTransformer<String, Boolean, String> entryTransformer = new Maps.EntryTransformer<String, Boolean, String>() {
            @Override
            public String transformEntry(String key, Boolean value) {
                //value为假，则key变大写
                return value ? key : key.toUpperCase();
            }
        };
        //输出：{value=VALUE, key=key}
        log.info("transformEntries -> {}",Maps.transformEntries(fromMap, entryTransformer));
    }
}