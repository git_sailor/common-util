package com.itasura.guava.Collections;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

/**
 * @author sailor wang
 * @date 2018/10/19 下午5:23
 * @description
 */
@Slf4j
public class ListsTest {

    @Test
    public void listsTest() {
        List list = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7);
        log.info("list -> {}", list);

        List<List<Integer>> subLists = Lists.partition(list, 3);//3个一组
        log.info("subLists -> {}", subLists);

        List<Integer> reversed = Lists.reverse(list);
        log.info("reversed -> {}",reversed);

        List<String> listStr = Lists.transform(list, new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return "_"+integer.toString()+"_";
            }
        });

        log.info("listStr -> {}",listStr);
    }
}