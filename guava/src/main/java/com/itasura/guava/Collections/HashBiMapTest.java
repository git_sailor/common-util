package com.itasura.guava.Collections;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * BiMap提供了一种新的集合类型，它提供了key和value的双向关联的数据结构
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:34
 * @description
 */
@Slf4j
public class HashBiMapTest {

    @Test
    public void findUserTest() {
        //在使用BiMap时，会要求Value的唯一性。如果value重复了则会抛出错误：java.lang.IllegalArgumentException
        HashBiMap<String, String> biMap = HashBiMap.create();
        biMap.put("小王", "王麻子");
        biMap.put("小刘", "刘能");
        biMap.put("小赵", "赵四");
        //biMap.put("老赵","赵四");//java.lang.IllegalArgumentException
        biMap.forcePut("老赵", "赵四");
        log.info("user -> {}", biMap);

        BiMap<String, String> inverseMap = biMap.inverse();
        log.info("inverseMap -> {}", inverseMap);

        log.info("王麻子小名 -> {}", inverseMap.get("王麻子"));
    }
}