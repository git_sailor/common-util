package com.itasura.guava.Collections;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/19 下午5:31
 * @description
 */
@Slf4j
public class HashMultisetTest {

    @Test
    public void hashMultisetTest() {
        Multiset<Integer> hashMultiSet = HashMultiset.create();
        hashMultiSet.add(1);
        hashMultiSet.add(2, 4);//放入4个2
        log.info("size -> {}, hashMultiSet -> {}", hashMultiSet.size(), hashMultiSet);
        int count = hashMultiSet.count(2);
        log.info("count -> {}", count);
        hashMultiSet.remove(2);//删除一个元素2
        log.info("size -> {}, hashMultiSet -> {}", hashMultiSet.size(), hashMultiSet);
        hashMultiSet.remove(2, 1);//删除指定个数的元素
        log.info("size -> {}, hashMultiSet -> {}", hashMultiSet.size(), hashMultiSet);
    }
}