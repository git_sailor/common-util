package com.itasura.guava.Collections;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Collection;

/**
 * 一个key值对应多个value
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:32
 * @description
 */
@Slf4j
public class HashMultimapTest {

    @Test
    public void hashMultimapTest() {
        //相同key的key-value pair 的value值是放在同一个数组中，相同的value会去重
        Multimap<Integer, Integer> hashMultimap = HashMultimap.create();
        hashMultimap.put(1, 2);
        hashMultimap.put(1, 2);
        hashMultimap.put(1, 3);
        hashMultimap.put(1, 4);
        hashMultimap.put(2, 3);
        hashMultimap.put(3, 3);
        hashMultimap.put(4, 3);
        hashMultimap.put(5, 3);
        log.info("hashMultimap -> {}", hashMultimap);

        //判断集合中是否存在key-value为指定值得元素
        Boolean b1 = hashMultimap.containsEntry(1, 2);
        log.info("b1 -> {}", b1);

        Boolean b2 = hashMultimap.containsEntry(1, 1);
        log.info("b2 -> {}", b2);

        //获取key的value集合
        Collection<Integer> coll = hashMultimap.get(1);
        log.info("coll -> {}", coll);

        //替换Multimap中指定key的值
        hashMultimap.replaceValues(2, Lists.newArrayList(1, 2, 3, 4, 5));
        Collection<Integer> coll1 = hashMultimap.get(2);
        log.info("coll1 -> {}", coll1);


    }
}