package com.itasura.guava.Collections;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.itasura.guava.BasicGuavaUtilities.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

/**
 * 流式编程
 *
 * @author sailor wang
 * @date 2018/10/29 3:51 PM
 * @description
 */
@Slf4j
public class FluentIterableTest {

    private static List<User> userList;
    static {
        User u1 = new User();
        u1.setName("A");
        u1.setAge(1);
        User u2 = new User();
        u2.setName("B");
        u2.setAge(13);
        User u3 = new User();
        u3.setName("C");
        u3.setAge(15);
        userList = Lists.newArrayList(u1,u2,u3);
    }

    @Test
    public void testFilter() {
        List<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5, 7, 8);
        Iterable<Integer> integers = FluentIterable.from(list).filter(new Predicate<Integer>() {
            @Override
            public boolean apply(Integer integer) {
                return integer > 7;
            }
        });
        log.info("integers -> {}", integers);

        Boolean flag = Iterables.contains(integers,8);
        log.info("flag -> {}",flag);
    }

    @Test
    public void transformTest(){
        List<String> userStrList = FluentIterable.from(userList).transform(new Function<User, String>() {
            @Override
            public String apply(User user) {
                return Joiner.on("#").join(user.getName(),user.getAge());
            }
        }).toList();
        log.info("userStrList -> {}",userStrList);
    }
}