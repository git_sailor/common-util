package com.itasura.guava.Collections;

import com.google.common.collect.MapMaker;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.ConcurrentMap;

/**
 * MapMaker 是用来构造 ConcurrentMap 的工具类
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:29
 * @description
 */
@Slf4j
public class MapMakerTest {

    @Test
    public void mapMakerTest() {
        // 构造ConcurrentHashMap，指定并发数 8
        ConcurrentMap<String,Integer> concurrentMap = new MapMaker().concurrencyLevel(8).makeMap();

        // 构造用各种不同 reference 作为 key 和 value 的 Map
        concurrentMap = new MapMaker().weakKeys().weakValues().makeMap();
    }
}