package com.itasura.guava.Collections;

import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Set;

/**
 * @author sailor wang
 * @date 2018/10/19 下午5:23
 * @description
 */
@Slf4j
public class SetsTest {

    Set<String> set1 = Sets.newHashSet("1", "2", "3", "4");
    Set<String> set2 = Sets.newHashSet("2", "3", "4", "5");

    // 差集
    @Test
    public void differenceTest() {
        Set<String> diffSet = Sets.difference(set1, set2);
        log.info("diffSet -> {}", diffSet);

        // 对等差集
        Set<String> symDiffSet = Sets.symmetricDifference(set1, set2);
        log.info("symDiffSet -> {}", symDiffSet);
    }

    // 并集
    @Test
    public void unionTest() {
        Set<String> unionSet = Sets.union(set1, set2);
        log.info("unionSet -> {}", unionSet);

    }

    // 交集
    @Test
    public void intersectionTest() {
        Set<String> interSet = Sets.intersection(set1, set2);
        log.info("interSet -> {}", interSet);
    }


}