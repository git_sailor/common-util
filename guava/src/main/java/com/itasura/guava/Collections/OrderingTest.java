package com.itasura.guava.Collections;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.itasura.guava.bean.City;
import com.itasura.guava.bean.CityByPopluation;
import com.itasura.guava.bean.CityByRainfall;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * 集合排序
 *
 * @author sailor wang
 * @date 2018/10/30 9:42 AM
 * @description
 */
@Slf4j
public class OrderingTest {

    @Test
    public void orderTest() {
        City shanghai = new City();
        shanghai.setName("上海");
        shanghai.setPopulation(24000000);
        City beijing = new City();
        beijing.setName("北京");
        beijing.setPopulation(20000000);

        List<City> cityList = Lists.newArrayList(shanghai, beijing);

        Ordering<City> ordering = Ordering.from(new CityByPopluation());
        cityList = ordering.sortedCopy(cityList);
        log.info("citylist -> {}", cityList);
    }

    /**
     * null值置前
     */
    @Test
    public void orderNullsFirstTest() {
        City shanghai = new City();
        shanghai.setName("上海");
        shanghai.setPopulation(24000000);
        City beijing = new City();
        beijing.setName("北京");
        beijing.setPopulation(20000000);

        List<City> cityList = Lists.newArrayList(shanghai, beijing, null);

        Ordering<City> ordering = Ordering.from(new CityByPopluation()).nullsFirst();
        cityList = ordering.sortedCopy(cityList);
        log.info("citylist -> {}", cityList);
    }

    /**
     * null值置后
     */
    @Test
    public void orderNullsLastTest() {
        City shanghai = new City();
        shanghai.setName("上海");
        shanghai.setPopulation(24000000);
        City beijing = new City();
        beijing.setName("北京");
        beijing.setPopulation(20000000);

        List<City> cityList = Lists.newArrayList(shanghai, beijing, null);

        Ordering<City> ordering = Ordering.from(new CityByPopluation()).nullsLast();
        cityList = ordering.sortedCopy(cityList);
        log.info("citylist -> {}", cityList);
    }

    @Test
    public void secondarySortTest() {
        City shanghai = new City();
        shanghai.setName("上海");
        shanghai.setPopulation(24000000);
        shanghai.setAverageRainfall(55.0);

        City beijing = new City();
        beijing.setName("北京");
        beijing.setPopulation(30000000);
        beijing.setAverageRainfall(45.0);
        List<City> cityList = Lists.newArrayList(shanghai, beijing);

        Ordering<City> ordering1 = Ordering.from(new CityByPopluation());
        Collections.sort(cityList, ordering1);

        log.info("cityList -> {}", cityList);

        log.info("\r\n");

        Ordering<City> ordering2 = Ordering.from(new CityByPopluation()).compound(new CityByRainfall());
        Collections.sort(cityList, ordering2);

        log.info("cityList -> {}", cityList);
    }

    /**
     * 检索
     */
    @Test
    public void retrievingTest() {
        City shanghai = new City();
        shanghai.setName("上海");
        shanghai.setPopulation(5);

        City beijing = new City();
        beijing.setName("北京");
        beijing.setPopulation(4);

        City guangzhou = new City();
        guangzhou.setName("广州");
        guangzhou.setPopulation(3);

        City hangzhou = new City();
        hangzhou.setName("杭州");
        hangzhou.setPopulation(2);

        City suzhou = new City();
        suzhou.setName("苏州");
        suzhou.setPopulation(1);

        List<City> cities = Lists.newArrayList(shanghai, beijing, guangzhou, hangzhou, suzhou);
        Ordering<City> ordering = Ordering.from(new CityByPopluation());

        List<City> topFive = ordering.greatestOf(cities, 5);
        log.info("topFive -> {}", topFive);
        log.info("\r\n");
        List<City> buttonFree = ordering.leastOf(cities, 3);
        log.info("buttonFree -> {}", buttonFree);

    }


}