package com.itasura.guava.Collections;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.itasura.guava.BasicGuavaUtilities.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 一个key值对应多个value
 * @author sailor wang
 * @date 2018/10/19 下午5:32
 * @description
 */
@Slf4j
public class ArrayListMultimapTest {

    @Test
    public void test(){
        String contryNameKey1 = "那个村";
        String contryNameKey2 = "这个村";
        //一个key多个values
        Multimap<String, User> multimap = ArrayListMultimap.create();
        for (int i=0;i<5;i++){
            User user = new User();
            user.setName("那个村村长，张富贵");
            user.setAge(i);
            multimap.put(contryNameKey1,user);
        }
        for (int i=0;i<5;i++){
            User user = new User();
            user.setName("这个村村长，张富贵");
            user.setAge(i);
            multimap.put(contryNameKey2,user);
        }
        log.info("那个村数据 -> {}",multimap.get(contryNameKey1));
        log.info("--------------------------------------------------------------");
        log.info("这个村数据 -> {}",multimap.get(contryNameKey2));
    }
}