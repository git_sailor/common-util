package com.itasura.guava.Collections;

import com.google.common.collect.ImmutableListMultimap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/30 9:34 AM
 * @description
 */
@Slf4j
public class immutableTest {

    @Test
    public void builderTest(){
        ImmutableListMultimap<Object, Object> multimap = ImmutableListMultimap.builder().put(1,"foo").putAll(2,"foo","Bar","Baz")
        .putAll(4,"Huey","Duey","Luey")
        .put(3,"single").build();
        log.info("multimap -> {}",multimap);
    }
}