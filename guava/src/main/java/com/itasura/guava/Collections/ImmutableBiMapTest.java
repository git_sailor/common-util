package com.itasura.guava.Collections;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * ImmutableMap是不可变集合，是线程安全的
 * immutable Objects就是那些一旦被创建，它们的状态就不能被改变的Objects，每次对他们的改变都是产生了新的immutable的对象，而mutable Objects就是那些创建后，状态可以被改变的Objects
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:34
 * @description
 */
@Slf4j
public class ImmutableBiMapTest {

    @Test
    public void immutableBiMapTest() {
        //支持各种类型的key-val,也可以强制泛型
        ImmutableBiMap immutableBiMap = ImmutableBiMap.of(1, "one", 2, 3, 4, Lists.newArrayList(1, 2, 3));

        log.info("immutableBiMap -> {}", immutableBiMap);

        log.info("immutableBiMap values -> {}", immutableBiMap.values());

        log.info("immutableBiMap -> {}", immutableBiMap.get(4));

        immutableBiMap = immutableBiMap.inverse();

        log.info("immutableBiMap inverse -> {}", immutableBiMap);

        log.info("immutableBiMap -> {}", immutableBiMap.get(Lists.newArrayList(1, 2, 3)));
    }
}