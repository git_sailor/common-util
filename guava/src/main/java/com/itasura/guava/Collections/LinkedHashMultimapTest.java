package com.itasura.guava.Collections;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * LinkedHashMultimap实现类与HashMultimap类的实现方法一样，唯一的区别是LinkedHashMultimap保存了记录的插入顺序
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:33
 * @description
 */
@Slf4j
public class LinkedHashMultimapTest {

    @Test
    public void linkedHashMultimapTest() {
        Multimap<Integer,Integer> linkedHashMultimap = LinkedHashMultimap.create();
        linkedHashMultimap.put(1, 2);
        linkedHashMultimap.put(1, 2);
        linkedHashMultimap.put(1, 3);
        linkedHashMultimap.put(1, 4);
        linkedHashMultimap.put(2, 3);
        linkedHashMultimap.put(3, 3);
        linkedHashMultimap.put(4, 3);
        linkedHashMultimap.put(5, 3);
        log.info(" linkedHashMultimap -> {}",linkedHashMultimap);
    }
}