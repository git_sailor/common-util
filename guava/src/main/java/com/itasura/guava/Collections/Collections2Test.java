package com.itasura.guava.Collections;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.itasura.guava.BasicGuavaUtilities.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
/**
 * 常用方法：
 * filter 过滤器
 * orderedPermutations 先将元素排序，在排列
 * permutations 直接排列
 * transform 变换器
 * @author sailor wang
 * @date 2018/10/19 下午5:28
 * @description
 */
@Slf4j
public class Collections2Test {

    private static List<User> userList;

    static {
        User user1 = new User();
        user1.setName("王富贵");
        user1.setAge(76);
        User user2 = new User();
        user2.setName("瞎仔");
        user2.setAge(20);
        User user3 = new User();
        user3.setName("小妖精");
        user3.setAge(18);
        User user4 = new User();
        user4.setName("饕餮");
        user4.setAge(1000);
        userList = Lists.newArrayList(user1,user2,user3,user4);
    }

    @Test
    public void filterTest(){

        Collection<User> monster = Collections2.filter(userList, new Predicate<User>() {
            @Override
            public boolean apply(User user) {
                return user.getAge() > 200;
            }
        });

        monster.forEach(mons ->{
            log.info("monster -> {}",mons.getName());
        });
    }

    @Test
    public void transferTest(){
        Collection<User> users = Collections2.transform(userList, new Function<User, User>() {
            @Override
            public User apply(User user) {
                if (user.getName().contains("王富贵")){
                    user.setName("嘿嘿，我是那个村村长：王富贵");
                }
                return user;
            }
        });
        log.info("users -> {}",users);
    }

    @Test
    public void orderedPermutationsTest(){
        log.info("排序前 userList -> {}",userList);
        Collection<List<User>> users = Collections2.orderedPermutations(userList);
        users.forEach(user -> {
            log.info("======= users ===== -> {}",user);
        });

        //没搞懂。。。
        log.info("--------------------使用比较器--------------------");
        Collection<List<User>> collection = Collections2.orderedPermutations(userList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getAge()>o2.getAge()?1:0;
            }
        });
        collection.forEach(user -> {
            log.info("------- users ------- -> {}",user);
        });


        Collection<List<User>> collection1 = Collections2.permutations(userList);
        log.info("permutations -> {}",collection1);
    }

}