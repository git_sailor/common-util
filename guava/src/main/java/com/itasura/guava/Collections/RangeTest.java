package com.itasura.guava.Collections;

import com.google.common.collect.Range;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.omg.CORBA.INTERNAL;

/**
 * @author sailor wang
 * @date 2018/10/19 下午5:24
 * @description
 */
@Slf4j
public class RangeTest {

    /**
     * 概念	表示范围	guava对应功能方法
     * (a..b)	{x | a < x < b}	open(C, C)
     * [a..b]	{x | a <= x <= b} 	closed(C, C)
     * [a..b)	{x | a <= x < b}	closedOpen(C, C)
     * (a..b]	{x | a < x <= b}	openClosed(C, C)
     * (a..+∞)	{x | x > a}	greaterThan(C)
     * [a..+∞)	{x | x >= a}	atLeast(C)
     * (-∞..b)	{x | x < b}	lessThan(C)
     * (-∞..b]	{x | x <= b}	atMost(C)
     * (-∞..+∞)	all values	all()
     */

    @Test
    public void rangeTest() {
        Range<Integer> openRange = Range.open(1, 10);
        log.info("openRange -> {}", openRange);
        Range<Integer> closeRange = Range.closed(1, 10);
        log.info("closeRange -> {}", closeRange);
        log.info("closedOpen -> {}", Range.closedOpen(1, 10));
        log.info("openClosed -> {}", Range.openClosed(1, 10));
        log.info("greaterThan -> {}", Range.greaterThan(10));
        log.info("atLeast -> {}", Range.atLeast(10));
        log.info("lessThan -> {}", Range.lessThan(10));
        log.info("atMost -> {}", Range.atMost(10));
        log.info("all -> {}", Range.all());
        log.info("closed -> {}", Range.closed(10, 10));
        log.info("closedOpen -> {}", Range.closedOpen(10, 10));

        log.info("{}, {}",openRange.contains(10),closeRange.contains(1));

    }
}