package com.itasura.guava.Collections;

import com.google.common.collect.EnumBiMap;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 用法和BiMap一样，key 与 value 都必须是 enum 类型
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:34
 * @description
 */
@Slf4j
public class EnumBiMapTest {

    @Test
    public void enumBiMapTest() {
        EnumBiMap<UserType, UserRole> userTypeMap = EnumBiMap.create(UserType.class, UserRole.class);
        userTypeMap.put(UserType.ADMIN, UserRole.ADMIN);
        userTypeMap.put(UserType.SALES, UserRole.SALES);
        userTypeMap.put(UserType.NORMAL, UserRole.NORMAL);
        log.info("userTypeMap -> {}", userTypeMap);
        //userTypeMap.put(UserType.ADMIN,UserRole.SALES);//管理员添加销售权限，java.lang.IllegalArgumentException: value already present
        userTypeMap.forcePut(UserType.ADMIN, UserRole.SALES);
        log.info("userTypeMap -> {}", userTypeMap);//强制添加会覆盖，SALES数据
    }
}

@Getter
enum UserType {
    ADMIN(0, "管理员"),
    SALES(1, "销售"),
    NORMAL(2, "普通用户"),

    ;
    private Integer code;
    private String msg;

    UserType(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

@Getter
enum UserRole {
    ADMIN(0, "管理员"),
    SALES(1, "销售"),
    NORMAL(2, "普通用户"),

    ;
    private Integer code;
    private String msg;

    UserRole(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}