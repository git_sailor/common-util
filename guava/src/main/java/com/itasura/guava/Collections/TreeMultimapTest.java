package com.itasura.guava.Collections;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * TreeMultimap类继承成了Multimap接口，它的所有方法跟HashMultimap一样，但是有一点不同的是该类实现了SortedSetMultimap接口，该接口会将存入的数据按照自然排序，默认是升序
 *
 * @author sailor wang
 * @date 2018/10/19 下午5:33
 * @description
 */
@Slf4j
public class TreeMultimapTest {
    @Test
    public void treeMultimapTest(){
        Multimap<Integer,Integer> treeMultimap = TreeMultimap.create();
        treeMultimap.put(1,1);
        treeMultimap.putAll(2, Lists.newArrayList(2,5,3,6,9,7,10));
        treeMultimap.putAll(3, Lists.newArrayList(208,58,93,64,90,71,101));

        log.info("treeMultimap -> {}",treeMultimap);
    }
}