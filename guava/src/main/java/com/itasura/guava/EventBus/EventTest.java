package com.itasura.guava.EventBus;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sailor wang
 * @date 2018/11/1 4:57 PM
 * @description
 */
@Data
@Slf4j
public class EventTest {

    private String message;

    public EventTest(String message) {
        this.message = message;
        log.info("event message -> {}", message);
    }

}