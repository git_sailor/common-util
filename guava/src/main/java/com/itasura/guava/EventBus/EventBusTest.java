package com.itasura.guava.EventBus;

import com.google.common.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/11/1 5:01 PM
 * @description
 */
@Slf4j
public class EventBusTest {

    @Test
    public void eventBusTest() {
        EventBus eventBus = new EventBus("test");
        EventListener eventListener = new EventListener();

        eventBus.register(eventListener);

        eventBus.post(new EventTest("事件1"));
        eventBus.post(new EventTest("事件2"));
        eventBus.post(new EventTest("事件3"));
        eventBus.post(new EventTest("事件4"));

        //如果EventBus发送的消息都不是订阅者关心的称之为Dead Event
        eventBus.post(new Integer(1));

        eventListener.getMessage();
    }
}