package com.itasura.guava.EventBus;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

/**
 * @author sailor wang
 * @date 2018/11/1 4:48 PM
 * @description
 */
@Slf4j
public class EventListener {

    private String message;

    @Subscribe
    public void listen(EventTest eventTest) {
        this.message = eventTest.getMessage();
        log.info("listen message -> {}", this.message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}