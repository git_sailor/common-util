package com.itasura.guava.FunctionalProgramming;

import com.google.common.base.Function;

/**
 * @author sailor wang
 * @date 2018/10/19 上午11:24
 * @description
 */
public class SqrtFunction implements Function<Double,Double> {
    @Override
    public Double apply(Double aDouble) {
        return Math.sqrt(aDouble);
    }
}