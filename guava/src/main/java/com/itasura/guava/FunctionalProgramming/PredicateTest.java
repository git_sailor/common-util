package com.itasura.guava.FunctionalProgramming;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.itasura.guava.BasicGuavaUtilities.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/19 下午3:21
 * @description
 */
@Slf4j
public class PredicateTest {

    @Test
    public void predicateTest(){
        Predicate<User> predicate = new Predicate<User>(){
            @Override
            public boolean apply(User user) {
                return user.getAge()<18;
            }
        };
        User young = new User();
        young.setAge(16);
        log.info("is young people -> {}",predicate.apply(young));
    }

    @Test
    public void predicatesTest(){
        Predicate<User> isNewUser = new Predicate<User>(){
            @Override
            public boolean apply(User user) {
                //..............此处省略数据库查询..............
                User u = null;//select User from db
                return u == null;
            }
        };
        Predicate<User> isYoung = new Predicate<User>() {
            @Override
            public boolean apply(User user) {
                return user.getAge() < 18;
            }
        };

        User u = new User();
        u.setAge(16);
        Predicate<User> newYoungUserPredicate = Predicates.and(isNewUser,isYoung);
        log.info("是否是年轻的新用户 -> {}",newYoungUserPredicate.apply(u));

        //.................................................
        //... Predicates.or()、Predicates.compose()等用法
        //.................................................
    }
}