**常用方法**
```
1.  V getIfPresent(Object key) 获取缓存中key对应的value，如果缓存没命中，返回null。
2.  V get(K key) throws ExecutionException 获取key对应的value，若缓存中没有，则调用LocalCache的load方法，从数据源中加载，并缓存。
3.  void put(K key, V value) 如果缓存有值，覆盖，否则，新增
4.  void putAll(Map m);循环调用单个的方法
5.  void invalidate(Object key); 删除缓存
6.  void invalidateAll(); 清楚所有的缓存，相当远map的clear操作。
7.  long size(); 获取缓存中元素的大概个数。为什么是大概呢？元素失效之时，并不会实时的更新size，所以这里的size可能会包含失效元素。
8.  CacheStats stats(); 缓存的状态数据，包括(未)命中个数，加载成功/失败个数，总共加载时间，删除个数等。
9.  asMap()方法获得缓存数据的ConcurrentMap快照
10. cleanUp()清空缓存
11. refresh(Key) 刷新缓存，即重新取缓存数据，更新缓存
12. ImmutableMap getAllPresent(Iterable keys) 一次获得多个键的缓存值
```

**核心类**
```
1. CacheBuilder：类，缓存构建器。构建缓存的入口，指定缓存配置参数并初始化本地缓存。 
2. CacheBuilder在build方法中，会把前面设置的参数，全部传递给LocalCache，它自己实际不参与任何计算。这种初始化参数的方法值得借鉴，代码简洁易读。
3. CacheLoader：抽象类。用于从数据源加载数据，定义load、reload、loadAll等操作。
4. Cache：接口，定义get、put、invalidate等操作，这里只有缓存增删改的操作，没有数据加载的操作。
5. AbstractCache：抽象类，实现Cache接口。其中批量操作都是循环执行单次行为，而单次行为都没有具体定义。
6. LoadingCache：接口，继承自Cache。定义get、getUnchecked、getAll等操作，这些操作都会从数据源load数据。
7. AbstractLoadingCache：抽象类，继承自AbstractCache，实现LoadingCache接口。
8. LocalCache：类。整个guava cache的核心类，包含了guava cache的数据结构以及基本的缓存的操作方法。
9. LocalManualCache：LocalCache内部静态类，实现Cache接口。 
10. 其内部的增删改缓存操作全部调用成员变量 localCache（LocalCache类型）的相应方法。
11. LocalLoadingCache：LocalCache内部静态类，继承自LocalManualCache类，实现LoadingCache接口。 
12. 其所有操作也是调用成员变量localCache（LocalCache类型）的相应方法。
13. CacheStats：缓存加载/命中统计信息。
```