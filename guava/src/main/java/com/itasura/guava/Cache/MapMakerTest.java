package com.itasura.guava.Cache;

import com.google.common.collect.MapMaker;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.ConcurrentMap;

/**
 * @author sailor wang
 * @date 2018/10/31 2:48 PM
 * @description
 */
@Slf4j
public class MapMakerTest {

    @Test
    public void concurrentMapTest() {
        ConcurrentMap<String, String> concurrentMap = new MapMaker().concurrencyLevel(2).weakValues().makeMap();
    }
}