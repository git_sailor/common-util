package com.itasura.guava.BasicGuavaUtilities;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/19 上午10:21
 * @description
 */
@Slf4j
public class ComparisonChainTest {

    @Test
    public void compareTest(){
        User u1 = new User();
        u1.setName("zhangsan");
        u1.setAge(18);
        u1.setMobile("15803456789");

        User u2 = new User();
        u2.setName("zhangsan");
        u2.setAge(18);
        u2.setMobile("15803456789");

        log.info("compare result -> {}",u1.compareTo(u2));

        u2.setMobile("15803456780");

        log.info("compare result -> {}",u1.compareTo(u2));
    }

}