package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/18 下午5:18
 * @description
 */
@Slf4j
public class StringsTest {
    @Test
    public void stringsTest() {
        log.info("Strings.isNullOrEmpty(\"\") -> {}", Strings.isNullOrEmpty(""));
        log.info("Strings.nullToEmpty(null) -> {}", Strings.nullToEmpty(null));
        log.info("Strings.nullToEmpty(\"a\") -> {}", Strings.nullToEmpty("a"));
        log.info("Strings.emptyToNull(\"\") -> {}", Strings.emptyToNull(""));
        log.info("Strings.emptyToNull(\"a\") -> {}", Strings.emptyToNull("a"));

        log.info("Strings.padStart(\"7\", 3, '0') -> {}", Strings.padStart("7", 3, '0'));
        log.info("Strings.padStart(\"2010\", 3, '0') -> {}", Strings.padStart("2010", 3, '0'));
        log.info("Strings.padStart(\"2010\", 3, '0') -> {}", Strings.padStart("2010", 30, '-'));

        log.info("Strings.padEnd(\"4.\", 5, '0') -> {}", Strings.padEnd("4.", 5, '0'));
        log.info("Strings.padEnd(\"2010\", 3, '!') -> {}", Strings.padEnd("2010", 5, '!'));
        log.info("Strings.padEnd(\"4.\", 5, '0') -> {}", Strings.padEnd("4.", 2, '0'));
        log.info("Strings.repeat(\"hei\", 2) -> {}", Strings.repeat("hei", 2));

        log.info("Strings.commonPrefix(\"aaab\", \"aac\") -> {}", Strings.commonPrefix("aaab", "aac"));
        log.info("Strings.commonSuffix(\"aaac\", \"aac\") -> {}", Strings.commonSuffix("aaac", "aac"));
    }
}