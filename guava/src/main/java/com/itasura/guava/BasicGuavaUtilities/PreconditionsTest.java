package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/18 下午5:19
 * @description
 */
@Slf4j
public class PreconditionsTest {

    // 没使用Preconditions的判断 java.lang.RuntimeException: 用户名或密码不能为空
    @Test
    public void loginA() {
        String userName = null;
        String password = null;
        if (userName == null || password == null) {
            throw new RuntimeException("用户名或密码不能为空");
        }
    }


    // 使用Preconditions的判断方式 java.lang.IllegalArgumentException: 用户名或密码不能为空
    @Test
    public void loginB() {
        String userName = null;
        String password = null;
        Preconditions.checkArgument(!(userName == null || password == null), "用户名或密码不能为空");

    }

    // 使用Preconditions的判断方式 java.lang.NullPointerException: 用户为空
    @Test
    public void loginC() {
        Object user = null;
        Preconditions.checkNotNull(user, "用户为空");
    }

    // 使用Preconditions的判断方式 java.lang.IllegalStateException: 操作失败
    @Test
    public void loginD() {
        Boolean success = false;
        Preconditions.checkState(success, "操作失败");
    }

    // java.lang.IndexOutOfBoundsException: 位置下表异常 (3) must be less than size (2)
    @Test
    public void loginE() {
        Preconditions.checkElementIndex(3, 2, "位置下表异常");
    }
}