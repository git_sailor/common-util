package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import lombok.Data;

import java.util.Comparator;

@Data
public class User implements Comparable<User> {
    private Long id;
    private String name;
    private Integer age;
    private String mobile;

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .omitNullValues()
                .add("name", name)
                .add("age", age)
                .add("mobile", mobile)
                .toString();
    }


    @Override
    public int compareTo(User user) {
        return ComparisonChain.start().compare(name,user.getName()).compare(age,user.getAge()).compare(mobile,user.getMobile()).result();
    }
}