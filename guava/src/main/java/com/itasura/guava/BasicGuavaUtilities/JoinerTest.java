package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author sailor wang
 * @date 2018/10/18 下午5:16
 * @description
 */
@Slf4j
public class JoinerTest {
    @Test
    public void joinerTest() throws IOException {
        List<String> list = Lists.newArrayList("one", "two", "three", null);
        //String str1 = Joiner.on("|").join(list);// 存在null值，会抛异常
        //log.info("str skipNulls -> {}",str1);

        //*********************************************
        String str = Joiner.on("|").skipNulls().join(list);//跳过null值
        log.info("str skipNulls -> {}", str);

        //*********************************************
        str = Joiner.on("|").useForNull("NULL").join(list);//null值替换为指定字符串
        log.info("str useForNull -> {}", str);

        //*********************************************
        StringBuilder sb = new StringBuilder();
        Joiner joiner = Joiner.on("|").skipNulls();
        joiner.appendTo(sb, "foo", "bar");
        log.info("sb -> {}", sb.toString());

        //*********************************************
        FileWriter fileWriter = new FileWriter(new File("/software/a.txt"));
        List<String> strList = Lists.newArrayList("hello", "world");//输出数据追加到file文件
        Joiner joiner1 = Joiner.on("#").useForNull(" ");
        joiner1.appendTo(fileWriter, strList);
        fileWriter.flush();

        //*********************************************
        Map<String, String> map = Maps.newHashMap();
        map.put("one", "1");
        map.put("two", "2");
        Joiner.MapJoiner mapJoiner = Joiner.on("&").withKeyValueSeparator("=");
        String params = mapJoiner.join(map);
        log.info("params -> {}", params);
    }
}