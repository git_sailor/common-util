package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Objects;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author sailor wang
 * @date 2018/10/18 下午5:48
 * @description
 */
@Slf4j
public class ObjectsTest {

    @Test
    public void userToStringTest() {
        User user = new User();
        user.setName("sailor");
        user.setAge(28);
        //user.setMobile("123456789");
        log.info("user -> {}", user);
    }

    @Test
    public void firstNonNullTest(){
        String str = null;
        String value = Objects.firstNonNull(str,"default value");
        log.info("value -> {}",value);
    }

    @Test
    public void hashCodeTest(){
        String name = "sailor";
        Integer age = 28;
        String mobile = "158000000";
        Integer hashCode = Objects.hashCode(name,age,mobile);
        log.info("hashCode -> {}",hashCode);
    }
}

