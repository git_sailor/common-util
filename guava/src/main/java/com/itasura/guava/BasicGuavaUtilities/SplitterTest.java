package com.itasura.guava.BasicGuavaUtilities;

import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Map;

/**
 * @author sailor wang
 * @date 2018/10/18 下午5:16
 * @description
 */
@Slf4j
public class SplitterTest {
    @Test
    public void splitterTest() {
        Iterable<String> list = Splitter.on('|').split("foo|bar|baz");
        log.info("list -> {}", list);


        Iterable<String> list1 = Splitter.on('|').trimResults().split("foo | bar |baz ");//去掉空格
        log.info("list1 -> {}", list1);


        Splitter splitter = Splitter.on('|');
        splitter.trimResults();
        //Result would still contain empty elements
        Iterable<String> parts = splitter.split("1|2|3|||");
        log.info("parts -> {}", parts);

        String startString = "Washington D.C=Redskins#New YorkCity=Giants#Philadelphia=Eagles#Dallas=Cowboys";
        Splitter.MapSplitter mapSplitter = Splitter.on("#").withKeyValueSeparator("=");
        Map<String, String> map = mapSplitter.split(startString);
        log.info("map -> {}， keys -> {}，values -> {}", map, map.keySet(), map.values());
    }
}