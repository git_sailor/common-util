package com.itasura.guava.bean;

import com.google.common.primitives.Doubles;

import java.util.Comparator;

/**
 * 平均降雨量比较器
 *
 * @author sailor wang
 * @date 2018/10/30 10:19 AM
 * @description
 */
public class CityByRainfall implements Comparator<City> {
    @Override
    public int compare(City city1, City city2) {
        return Doubles.compare(city1.getAverageRainfall(), city2.getAverageRainfall());
    }
}