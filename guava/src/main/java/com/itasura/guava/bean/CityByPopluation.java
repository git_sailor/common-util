package com.itasura.guava.bean;

import com.google.common.primitives.Ints;

import java.util.Comparator;

/**
 * 人口比较器
 *
 * @author sailor wang
 * @date 2018/10/30 9:42 AM
 * @description
 */
public class CityByPopluation implements Comparator<City> {

    @Override
    public int compare(City o1, City o2) {
        return Ints.compare(o1.getPopulation(), o2.getPopulation());
    }
}