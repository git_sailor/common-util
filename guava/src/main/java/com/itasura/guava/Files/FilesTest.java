package com.itasura.guava.Files;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author sailor wang
 * @date 2018/11/1 5:22 PM
 * @description
 */
@Slf4j
public class FilesTest {

    @Test
    public void writeFile() throws IOException {
        String str = "To be, or not to be";
        File file = new File("/data/file.txt");
        Files.write(str, file, Charsets.UTF_8);

        List<String> lines = Files.readLines(file, Charsets.UTF_8);
        log.info("lines -> {}", lines);
    }

    @Test
    public void copyFile() throws IOException {
        File file = new File("/data/file.txt");
        File copyFile = new File("/data/copyFile.txt");
        Files.copy(file, copyFile);

        List<String> lines = Files.readLines(copyFile, Charsets.UTF_8);
        log.info("copy file lines -> {}", lines);
    }

    @Test
    public void moveFile() throws IOException {
        File file = new File("/data/file.txt");
        File newFile = new File("/data/newFile.txt");
        Files.move(file, newFile);

        List<String> lines = Files.readLines(newFile, Charsets.UTF_8);
        log.info("new file lines -> {}", lines);
    }

    @Test
    public void hashFile() throws IOException {
        File newFile = new File("/data/newFile.txt");

        HashCode hashCode = Files.hash(newFile, Hashing.md5());
        log.info("hashcode -> {}", hashCode.hashCode());
    }

    @Test
    public void appendFile() throws IOException {
        String str = " ,that is the question";
        File newFile = new File("/data/newFile.txt");

        Files.append(str, newFile, Charsets.UTF_8);

        List<String> lines = Files.readLines(newFile, Charsets.UTF_8);
        log.info("append file lines -> {}", lines);

    }

    // Files.asByteSource方法为File对象创建ByteSource
    @Test
    public void createByteSourceFromFileTest() throws IOException {
        File file = new File("/data/newFile.txt");
        ByteSource byteSource = Files.asByteSource(file);
        byte[] readByte = byteSource.read();
        log.info("byteSource -> {}", byteSource);
        assertThat(readByte, is(Files.toByteArray(file)));
    }

    //ByteSink类表示一个可写的字节。我们可以将字节写入一个文件或另一个字节数组
    @Test
    public void createFileBySinkTest() throws IOException {
        File dest = new File("/data/test1.txt");
        dest.deleteOnExit();

        ByteSink byteSink = Files.asByteSink(dest);
        File file = new File("/data/test2.txt");
        byteSink.write(Files.toByteArray(file));

        assertThat(Files.toByteArray(dest), is(Files.toByteArray(file)));
    }


    // 从ByteSource 向ByteSink 复制, 通过ByteSource和ByteSink类展示一个从ByteSource实例到ByteSink实例复制底层字节的例子
    @Test
    public void copyToByteSinkTest() throws IOException {
        File dest = new File("/data/test1.txt");
        dest.deleteOnExit();
        File source = new File("/data/test2.txt");
        ByteSource byteSource = Files.asByteSource(source);
        ByteSink byteSink = Files.asByteSink(dest);
        byteSource.copyTo(byteSink);
        assertThat(Files.toByteArray(dest), is(Files.toByteArray(source)));
    }


}