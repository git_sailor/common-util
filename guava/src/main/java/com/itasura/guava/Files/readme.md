Guava提供了 InputSupplier 和 OutputSupplier接口，用于提供InputStreams/Readers 或OutputStreams/Writers的处理。我们将在接下来的文章中，看到Guava为我们提供的这些便利，Guava通常会在打开、刷新、关 闭资源时使用这些接口。 

    Sources 和 Sinks
Guava I/O对应于文件的读写分别提出来Sources 和Sinks 的概念，Sources 和Sinks不是那些 流，readers或writers对象，但是提供相同的作用。Sources 和Sinks 对象可以通过下面两种方式使用：

我们可以通过提供者检索底层流。每次提供者返回一个流，它是一个完全新的实例，独立于任何其他可能已经返回的实例。检索底层流对象的调用者负责关闭流。

提供了一些基本的便利的方法用于执行我们期望的基本的操作，如读取流或写入流。当通过Sources 和 Sinks执行读取和写入时，打开和关闭流操作交由我们处理。

Sources有两种类型：ByteSource 和 CharsSource。同样，Sinks也有两种类型：ByteSink 和 CharSink。各自的Sources和Sinks类提供类似的功能，它们方法的差异只取决于我们使用的是字符还是原始字节。Files类提供了几种方 法来通过ByteSink和CharSink类操作文件。我们可以通过Files类提供的静态工厂方法来创建ByteSource、ByteSink、 CharSource、CharSink实例。在我们的例子中，我们将专注于ByteSource和ByteSink对象，CharSource和 CharSink对象与之相似，只是使用的是字符。