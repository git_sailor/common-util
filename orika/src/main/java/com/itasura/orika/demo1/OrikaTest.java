package com.itasura.orika.demo1;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * 对象拷贝 属性名相同
 *
 * @author sailor wang
 * @date 2018/10/16 下午2:54
 * @description
 */
@Slf4j
public class OrikaTest {
    public static void main(String[] args) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        // MapperFacade执行实际的映射工作
        MapperFacade mapperFacade = mapperFactory.getMapperFacade();

        User user = User.valueOf("张三", 18);

        CopyUser copyUser = mapperFacade.map(user, CopyUser.class);
        log.info("copyUser -> {}", copyUser);

        User user1 = mapperFacade.map(copyUser,User.class);
        log.info("user1 -> {}",user1);

    }
}