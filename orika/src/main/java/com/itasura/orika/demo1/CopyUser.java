package com.itasura.orika.demo1;

import lombok.Data;

/**
 * @author sailor wang
 * @date 2018/10/16 下午4:12
 * @description
 */
@Data
public class CopyUser {
    private String name;
    private Integer age;
}