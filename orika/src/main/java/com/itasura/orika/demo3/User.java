package com.itasura.orika.demo3;

import lombok.Data;

/**
 * @author sailor wang
 * @date 2018/10/16 下午4:12
 * @description
 */
@Data
public class User {
    private String name;
    private Integer age;
    private String pwd;

    public User(String name, Integer age, String pwd) {
        this.name = name;
        this.age = age;
        this.pwd = pwd;
    }

    public static User valueOf(String name, Integer age, String pwd){
        return new User(name,age,pwd);
    }
}