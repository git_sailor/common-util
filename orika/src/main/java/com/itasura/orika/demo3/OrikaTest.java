package com.itasura.orika.demo3;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * 双向映射
 *
 * @author sailor wang
 * @date 2018/10/16 下午2:54
 * @description
 */
@Slf4j
public class OrikaTest {
    public static void main(String[] args) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(User.class, CopyUser.class)//A ClassMapBuilder<User, CopyUser>
                .exclude("pwd")
                .field("age", "aeg")
                .byDefault()//两个类上的其余字段都应该按名称映射到字段。
                .register();//用MapperFactory注册映射

        // BoundMapperFacade旨在与一对类型进行映射，而不需要进行进一步的定制。它为使用标准的MapperFacade提供了更好的性能。它有相同的用法，加上一个反向映射的mapReverse方法
        BoundMapperFacade<User, CopyUser> mapperFacade = mapperFactory.getMapperFacade(User.class, CopyUser.class);

        User user = User.valueOf("张三丰", 88,"我是密码");

        // 以下信息不会拷贝pwd，除非去掉 exclude("pwd")
        CopyUser copyUser = mapperFacade.map(user);
        log.info("copyUser -> {}", copyUser);

        User User1 = mapperFacade.mapReverse(copyUser);

        log.info("User1 -> {}", User1);
    }
}