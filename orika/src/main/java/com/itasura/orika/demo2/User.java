package com.itasura.orika.demo2;

import lombok.Data;

/**
 * @author sailor wang
 * @date 2018/10/16 下午4:12
 * @description
 */
@Data
public class User {
    private String name;
    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public static User valueOf(String name, Integer age){
        return new User(name,age);
    }
}