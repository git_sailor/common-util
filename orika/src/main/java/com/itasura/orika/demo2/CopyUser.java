package com.itasura.orika.demo2;

import lombok.Data;

/**
 * @author sailor wang
 * @date 2018/10/16 下午4:12
 * @description
 */
@Data
public class CopyUser {
    private String name;
    private Integer aeg;// 模仿字段写错
}