package com.itasura.orika.demo2;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.List;

/**
 * 对象拷贝 属性名不同[对象、集合]
 *
 * @author sailor wang
 * @date 2018/10/16 下午2:54
 * @description
 */
@Slf4j
public class OrikaTest {
    public static void main(String[] args) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(User.class, CopyUser.class)//A ClassMapBuilder<User, CopyUser>
                .field("age", "aeg")// 字段映射
                .mapNulls(false)// 忽略null值映射
                .byDefault()//两个类上的其余字段都应该按名称映射到字段。
                .register();//用MapperFactory注册映射

        User user = User.valueOf("张三", 18);
        CopyUser copyUser = mapperFactory.getMapperFacade().map(user,CopyUser.class);

        log.info("copyUser -> {}", copyUser);

        User user1 = User.valueOf("令狐冲", 23);
        User user2 = User.valueOf("东方不败", 18);
        List<User> list = Lists.newArrayList(user1,user2);
        List<CopyUser> copyUserList = mapperFactory.getMapperFacade().mapAsList(list,CopyUser.class);
        log.info("copyUserList -> {}",copyUserList);
    }
}