package com.itasura.orika.demo4;

import com.google.common.collect.Lists;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class BeanMapper {
    private static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    public BeanMapper() {

    }

    static {
        StudentOrikaMapper studentOrikaMapper = new StudentOrikaMapper();
        mapperFactory.registerMapper(studentOrikaMapper);
    }

    public static <T> T map(Object source, Class<T> destinationClass) {
        return source == null ? null : mapperFactory.getMapperFacade().map(source, destinationClass);
    }

    public static <T> List<T> mapList(Collection sourceList, Class<T> destinationClass) {
        if (sourceList == null) {
            return null;
        } else {
            List<T> destinationList = Lists.newArrayList();
            Iterator var3 = sourceList.iterator();

            while(var3.hasNext()) {
                Object sourceObject = var3.next();
                T destinationObject = mapperFactory.getMapperFacade().map(sourceObject, destinationClass);
                destinationList.add(destinationObject);
            }

            return destinationList;
        }
    }

    public static void copy(Object source, Object destinationObject) {
        if (source != null && destinationObject != null) {
            mapperFactory.getMapperFacade().map(source, destinationObject);
        }
    }

}