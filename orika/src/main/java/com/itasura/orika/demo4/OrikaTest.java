package com.itasura.orika.demo4;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 整理优化
 *
 * @author sailor wang
 * @date 2018/10/16 下午4:47
 * @description
 */
@Slf4j
public class OrikaTest {

    /**
     * 但是注意，因为两个类之间存在差异(两个属性有不同的名称，相同的概念、地址和不同的类型) mapper没有工作
     * Orika提供了自定义转换器和映射器 StudentOrikaMapper
     * StudentOrikaMapper studentOrikaMapper = new StudentOrikaMapper();
     * mapperFactory.registerMapper(studentOrikaMapper);
     */

    public static void main(String[] args) {
        Student student = Student.builder().id(1L).name("小李飞刀").address("幽灵古墓").mobile("123456").build();
        // 正常对象拷贝
        StudentDto studentDto = BeanMapper.map(student, StudentDto.class);
        log.info("bean 拷贝到 dto，studentDto -> {}", studentDto);

        Student student1 = BeanMapper.map(studentDto, Student.class);
        log.info("dto 拷贝到 bean，student1 -> {}", student1);

        Student student2 = Student.builder().id(2L).name("杨过").address("xxx").mobile("123456").build();
        Student student3 = Student.builder().id(3L).name("小龙女").address("xxx").mobile("123456").build();

        // 正常集合拷贝
        List<StudentDto> studentDtoList = BeanMapper.mapList(Lists.newArrayList(student2, student3), StudentDto.class);
        log.info("studentDtoList -> {}", studentDtoList);

        // 对象实体拷贝
        Student student4 = Student.builder().id(2L).name("我是真品").address("xxx").mobile("123456").build();
        Student student5 = new Student();
        BeanMapper.copy(student4, student5);
        log.info("student5 -> {}", student5);


    }
}