package com.itasura.orika.demo4;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

/**
 * 自定义映射器
 * @author sailor wang
 * @date 2018/10/16 下午6:05
 * @description
 */
public class StudentOrikaMapper extends CustomMapper<Student, StudentDto> {
    @Override
    public void mapAtoB(Student student, StudentDto studentDto, MappingContext context) {
        studentDto.setId(student.getId());
        studentDto.setName(student.getName());
        studentDto.setAddress(student.getAddress());
        studentDto.setTel(student.getMobile());
        //super.mapAtoB(student, studentDto, context);
    }

    @Override
    public void mapBtoA(StudentDto studentDto, Student student, MappingContext context) {
        student.setId(studentDto.getId());
        student.setName(studentDto.getName());
        student.setAddress(studentDto.getAddress());
        student.setMobile(studentDto.getTel());
        //super.mapBtoA(studentDto, student, context);
    }
}