package com.itasura.jodatime;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Locale;

/**
 * @author sailor wang
 * @date 2018/10/17 下午3:56
 * @description
 */
public class Main {

    // DateTime 当前系统所在时区的当前时间，精确到毫秒
    public static void dateTimeTest() {
        DateTime dateTime1 = new DateTime();
        System.out.println(dateTime1);

        DateTime dateTime2 = new DateTime(2018, 10, 17, 16, 06, 8);// 年 月 日 时 分 秒
        System.out.println(dateTime2);
        System.out.println(dateTime2.getMillis());// 1539763568000L

        DateTime dateTime3 = new DateTime(1539763568000L);
        System.out.println(dateTime3);

        DateTime dateTime4 = new DateTime(new Date());
        System.out.println(dateTime4);

        DateTime dateTime5 = new DateTime("2018-02-15T00:00:00.000+08:00");
        System.out.println(dateTime5);
    }

    // with开头的方法
    public static void dateTimeWithTest() {
        printLine();
        DateTime dateTime1 = new DateTime();
        System.out.println(dateTime1);// 2018-10-17T16:11:18.993+08:00

        System.out.println("withYear " + dateTime1.withYear(2017)); //2017-10-17T16:11:54.975+08:00
        System.out.println("withMonth " + dateTime1.withMonthOfYear(11)); //2018-11-17T16:12:38.664+08:00
        System.out.println("withDayOfYear " + dateTime1.withDayOfYear(32)); //2018-02-01T16:20:10.889+08:00
        System.out.println("withDayOfMonth " + dateTime1.withDayOfMonth(10)); //2018-10-10T16:20:52.075+08:00
        // withDayOfWeek 参数范围：1-7
        System.out.println("withDayOfWeek " + dateTime1.withDayOfWeek(1)); //2018-10-21T16:21:34.430+08:00
    }

    // plus/minus开头的方法
    public static void dateTimePMTest() {
        printLine();
        DateTime now = DateTime.now();
        System.out.println("now -> " + now);// 2018-10-17T16:28:24.117+08:00
        DateTime tomorrow = now.plusDays(1);
        System.out.println("tomorrow -> " + tomorrow);// 2018-10-18T16:28:39.808+08:00

        DateTime lastMonth = now.minusMonths(1);
        System.out.println("lastMonth -> " + lastMonth);// 2018-09-17T16:29:14.919+08:00
    }

    // datetime text 用法
    public static void dateTimeText() {
        printLine();
        DateTime now = new DateTime();
        System.out.println("now -> " + now);// 2018-10-17T16:32:38.196+08:00
        String text = now.monthOfYear().getAsText(); // 十月
        System.out.println(text);
        text = now.monthOfYear().getAsText(Locale.KOREAN); // 10월
        System.out.println(text);
        text = now.dayOfWeek().getAsText(); // Wed
        System.out.println(text);
        text = now.dayOfWeek().getAsShortText(Locale.ENGLISH); // 星期三
        System.out.println(text);
    }

    // 四舍五入
    public static void round() {
        printLine();
        DateTime now = new DateTime();
        System.out.println("now -> " + now);// 2018-10-17T16:39:42.870+08:00
        DateTime ceiling = now.dayOfWeek().roundCeilingCopy();//进
        System.out.println("ceiling -> " + ceiling);//2018-10-18T00:00:00.000+08:00
        DateTime floor = now.dayOfWeek().roundFloorCopy();//舍
        System.out.println("floor -> " + floor);//2018-10-17T00:00:00.000+08:00

        floor = now.minuteOfDay().roundFloorCopy(); // 2018-10-17T16:47:00.000+08:00
        System.out.println("floor -> " + floor);
        floor = now.secondOfMinute().roundFloorCopy(); // 2018-10-17T16:47:11.000+08:00
        System.out.println("floor -> " + floor);

    }

    // Interval和Period
    public static void interval() {
        printLine();
        DateTime now = new DateTime();
        System.out.println("now -> " + now);// 2018-10-17T16:54:16.709+08:00
        DateTime plusPeriod = now.plus(Period.days(1));
        System.out.println("plusPeriod -> " + plusPeriod);// 2018-10-18T16:54:16.709+08:00
        DateTime plusDuration = now.plus(new Duration(24L * 60L * 60L * 1000L));
        System.out.println("plusDuration -> " + plusDuration);

        DateTime begin = new DateTime("2017-02-01");
        DateTime end = new DateTime("2018-05-01");
        //计算区间毫秒数
        Duration d = new Duration(begin, end);
        long millis = d.getMillis();
        System.out.println("millis -> "+millis);

        //计算区间天数
        Period p = new Period(begin, end, PeriodType.days());
        int days = p.getDays();
        System.out.println("days -> "+days);

        //计算特定日期是否在该区间内
        Interval interval = new Interval(begin, end);
        boolean contained = interval.contains(new DateTime("2016-03-01"));

        System.out.println("contained -> "+contained);
    }

    // 本地日期
    public static void localDateTest() {
        printLine();
        LocalDate start = new LocalDate(2018, 10, 17);
        System.out.println(start);
        LocalDate end = new LocalDate(2018, 10, 19);
        System.out.println(end);
        int days = Days.daysBetween(start, end).getDays();
        System.out.println("days -> " + days);
    }

    // 日期格式
    public static void formatterTest() {
        printLine();
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        //时间解析
        DateTime dateTime = DateTime.parse("2018-12-03 17:19:20", format);
        String string_u = dateTime.toString("yyyy/MM/dd HH:mm:ss EE");
        System.out.println(string_u);

        //格式化带Locale，输出==> 2018年12月03日 17:19:20 Mon
        String string_c = dateTime.toString("yyyy年MM月dd日 HH:mm:ss EE", Locale.ENGLISH);
        System.out.println(string_c);
    }

    // 时区
    public static void dateZone() {
        printLine();
        //默认设置为日本时间
        DateTimeZone.setDefault(DateTimeZone.forID("Asia/Tokyo"));
        DateTime dt1 = new DateTime();
        System.out.println(dt1.toString("yyyy-MM-dd HH:mm:ss"));

        //伦敦时间
        DateTime dt2 = new DateTime(DateTimeZone.forID("Europe/London"));
        System.out.println(dt2.toString("yyyy-MM-dd HH:mm:ss"));
    }

    // 日期比较
    public static void compare(){
        printLine();
        DateTime d1 = new DateTime("2018-10-01");
        DateTime d2 = new DateTime("2018-12-01");
        //和系统时间比
        boolean b1 = d1.isAfterNow();
        System.out.println("isAfterNow -> "+b1);
        boolean b2 = d1.isBeforeNow();
        System.out.println("isBeforeNow -> "+b2);
        boolean b3 = d1.isEqualNow();
        System.out.println("isEqualNow -> "+b3);

        //和其他日期比
        boolean f1 = d1.isAfter(d2);
        System.out.println("isAfter -> "+f1);
        boolean f2 = d1.isBefore(d2);
        System.out.println("isBefore -> "+f2);
        boolean f3 = d1.isEqual(d2);
        System.out.println("isEqual -> "+f3);

    }

    public static void main(String[] args) {
        dateTimeTest();
        dateTimeWithTest();
        dateTimePMTest();
        dateTimeText();
        round();
        interval();
        localDateTest();
        formatterTest();
        dateZone();
        compare();
    }

    private static void printLine() {
        System.out.println("----------------------------------------------");
    }
}